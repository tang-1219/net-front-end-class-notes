## 不熟悉路径（重点）
```js
import fs from 'fs';
import { fileURLToPath } from 'url';
import {dirname} from 'path';

//获取给定路径下的js文件（这里的js文件通常就是控制器文件）
export function findRouter(conPath){
    let currentPath = conPath || './controller';
    let files= fs.readdirSync(currentPath);
    let resFiles=files.filter(item=>{
        return item .endsWith('.js') && item!=='index.js';
    })
    return resFiles

}

//传入js文件数组和路由实例，注册所有按给定格式定义的路由
export function registryRouter(ctx,router){
    let currentPath='../controller';
    files.forEach(async file =>{
        let filePath = currentPath + '/'+file;
        let tmpModule = await import(filePath);
        
        let tmpObj = tmpModule.default;
        for(let item in tmpOBJ){
          let tmpArr=item.split(' '); 
          let method= tmpArr[0];
          let path = tmpArr[1];
          let fn = tmpObj[item];
          router[method](path,fn); 
        }
    })
}
//获取给定url地址的完整的文件名
export function __filename(url){
    let filename= fileURLToPath(url);
    let dname=dirname(filename);
    return filename;

}
//获取给定url地址的完整路径，不包含文件名
export function __dirname(url){
    let filename=__filename(url);
    let dname = dirname(filename);
    return dname;
}

//获取给定完成路径下的简短文件名，不含路径和后缀名
export function getFileName(filePath){
    let arr = filePath.split('\\');
    let fileName=arr[arr.length - 1];
    let fName=fileName.split('.');
    return fName[0];
}


```