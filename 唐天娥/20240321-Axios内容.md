## Axios

### 解决浏览器的跨域问题的办法
- 1.从后端入手解决
- 2.从前端入手，使用代理的方法，让前后端同源，即可解决

### 了解下跨越的产生
- 1.本质上，其实就是浏览器为了安全，会阻止非同源的请求

### 同源是指什么
#### 一个请求包含三要素：
- 1.协议
- 2.主机名（域名）
- 3.端口号，如：http://qq.com:8000/abc

- 同源的意思，就是请求三要素，必须完全一致，否则就是非同源

```js
//允许所有的跨域请求
ctx.set('Access-Controll-Allow-Origin','*')
//后面的值，不能是小写，必须是大写
ctx.set('Access-Controll-Allow-Methods','GET,POST,PUT,DELETE,OPTIONS')
//允许指定请求头
ctx.set('Access-Controll-Allow-Headers','Content-Type')
ctx.status=200;
//执行下一个中间件
await next();
```

### 使用封装的中间件，成功解决跨域问题
```js
app.use(cors())
```

### 对于"*"的使用
```js
export default{
    //设置跨域的域名，如果允许所有则填'*'，如果需要配合cookies,则需要填具体的请求地址，如：http://localhost:/3000
    cors:'*'
}
```