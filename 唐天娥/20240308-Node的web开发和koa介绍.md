## web开发
- 在Node.js诞生后的短短几年里，出现了无数种Web框架、ORM框架、模版引擎、测试框架、自动化构建工具

用Node.js开发Web服务器端，有几个显著的优势：

+ 一是后端语言也是JavaScript，以前掌握了前端JavaScript的开发人员，现在可以同时编写后端代码；

+ 二是前后端统一使用JavaScript，就没有切换语言的障碍了；

+ 三是速度快，非常快！这得益于Node.js天生是异步的

## Koa
- koa是Express的下一代基于Node.js的web框架
- Express是第一代最流行的web框架，它对Node.js的http进行了封装