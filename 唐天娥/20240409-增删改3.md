```js
在edit.js文件里写：

function btnSave()
{
    let director=$('[name=director]').val();
    let actor=$('[name=actor]').val();
    let score=$('[name=score]').val();
    let flag=$('[name=flag]').val();

    let obj={director,actor,score,flag};

    let params=getQueryString();
    let id=params.id*1;
    if(id>0)
    {
        axios.put(`http://localhost:3000/films/${id}`,obj).then(res=>
        {
            let data=res.data;
            if(data.code===1000)
            {
                location.href='./index.html';
            }
            else
            {
                alert(data.msg);
            };
        });
    }
    else
    {
        axios.post('http://localhost:3000/films',obj).then(res=>
        {
            let data=res.data;
            if(data.code===1000)
            {
                location.href='./index.html';
            }
            else
            {
                console.log(data.msg);
            };
        });
    };
};

function getQueryString(str=window.location.search)
{
    const reg=/[?&]?([^=]+)=([^&]*)/g;
    const params={};
    str.replace(reg,(_,k,v)=>params[k]=v);
    return params;
}
在base.js文件里写：

function btnEdit(id)
{
    location.href='./edit.html?id='+id;
};


```